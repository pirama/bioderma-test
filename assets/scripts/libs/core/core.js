      $(document).ready(function() {
          if ('ontouchstart' in document.documentElement) { // or whatever "is this a touch device?" test we want to use
              $('body').css('cursor', 'pointer');
          }

          $('.points-level.active').prev().addClass('active-prev');
          $('.comment-toggle').on('shown.bs.collapse', function() {
              $(this).toggleClass('active');
          });
          $('.like').on('click', function(e) {
              e.preventDefault();
              $(this).toggleClass('active');
          });
          $('.collapse-close').click(function(e) {
              e.preventDefault();
              var href = $(this).attr('href');
              $(href).collapse('hide');
          });
          $('#mobileNav').on('shown.bs.collapse', function() {
              $('.wrapper').addClass('open');
          });
          $('#mobileNav').on('hide.bs.collapse', function() {
              $('.wrapper').removeClass('open');
          });
          $('.modal').on('shown.bs.collapse', function() {
              $('.wrapper').removeClass('open');
          });
          $('.jumbotron .close').on('click', function() {
              $(this).parents('.jumbotron').fadeOut();
          });
          if ($("html").width() < 500) {
              $('.modal').on('shown.bs.modal', function(e) {
                  $('.wrapper').addClass('open');
              });
              $('.modal').on('hide.bs.modal', function(e) {
                  $('.wrapper').removeClass('open');
              });
          }


          $('.step2 a').on('click', function(e) {
              e.preventDefault();
              $(this).parents('.step').find('.next-btn').addClass('active');
          });
          $('.step .btn-input').on('click', function(e) {
              e.preventDefault();
              $(this).parents('.step').find('.next-btn').addClass('active');
          });

          $('.step .next-btn').on('click', function(e) {
              e.preventDefault();
              $(this).parents('.step').removeClass('active')
              var str = $(this).parents('.step').attr('id')
              str = str.substr(str.length - 1);
              var intValue = parseInt(str) + 1;
              $('#step' + intValue).addClass('active');
          });
          if ($(window).width() < 400) {
              $('.step1 .form-control').attr('placeholder', 'Produkto kodas')
          }
          $(function() {
              $('[data-toggle="tooltip"]').tooltip()
          })

          if ($('select').hasClass('form-control')) {
              $('select').select2();
          }
          $(document).on('click', '#toReply', function(event) {
              event.preventDefault();
              console.log($.attr(this, 'href'));

              $('html, body').animate({
                  scrollTop: $($.attr(this, 'href')).offset().top
              }, 500);
              $('#replyTextarea').focus();
          });

          var serachValues = [
              { value: 'Dolor sit amet, Lorem ipsum consectetur adipiscing elit. Nam eu sollicitudin metus, nec elementum...', data: 'Dolor sit amet, Lorem ipsum consectetur adipiscing elit. Nam eu' },
              { value: 'Dolor sit amet', data: 'Dolor sit amet' },
              { value: 'adipiscing elit. Nam eu sollicitudin metus, nec elementum telluselit. Nam eu sollelit. Nam eu soll...', data: 'adipiscing elit. Nam eu sollicitudin metus, nec elementum telluselit. Nam eu sollelit. Nam eu soll...' },
              { value: 'Dolor sit amet', data: 'Dolor sit amet' },
              { value: 'Dolor sit amet', data: 'Dolor sit amet' }
          ];
          if ($("input").is("#start-date")) {
              $('#autocomplete').autocomplete({
                  lookup: serachValues,
                  onSelect: function(suggestion) {
                      alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                  }
              });
          }


          $('div[data-readmore]').hide().each(function() {
              var open_text = $(this).data('open-text');
              console.log($(this).prev());
              open_text = typeof open_text !== 'undefined' ? open_text : 'Read More';
              $(this).prev().append('<a data-readmore-toggle href="#" class="link">' + open_text + '</a>');

          });

          $('[data-readmore-toggle]').click(function(e) {
              e.preventDefault();

              var open_text = $(this).siblings('div[data-readmore]').data('open-text');
              var close_text = $(this).siblings('div[data-readmore]').data('close-text');

              if (typeof open_text == 'undefined') { open_text = "Read More" }
              if (typeof close_text == 'undefined') { close_text = "Show Less" }

              if ($(this).text() == open_text) {
                  $(this).html(close_text).parent().next('div[data-readmore]').show().after(this);
              } else {
                  $(this).html(open_text).prev('div[data-readmore]').hide().prev().append(this);

              }
          });
          $('.add-link').click(function(e) {
              e.preventDefault();
              var content = $('.invite-friends-item:first');
              // console.log(content);
              // $('.invite-friends-item').after($(".invite-friends-item"));
              $(content).clone().appendTo(".invite-friends-list");
          });
          $('#registration').click(function(e) {
              $(this).parents('.modal-form-content').hide();
              $(this).parents('.modal').find('.modal-success').show();
          });
          $('textarea').autoResize();

      });
